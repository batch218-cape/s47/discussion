// The "document" refers to the whole webpage
// The "querySelector" is used to select a specific object (HTML elements) from the webpage(document)

// Alternatively, we can use getElement functions to retrieve the elements
// const txtFirstName = document.getElementById('txt-first-name')

const txtFirstName = document.querySelector('#text-first-name');
const spanFullName = document.querySelector('#span-full-name');

//The "addEventListener" is a function that takes two arguments 
//keyup - string identifying the event
//event => {} - function that the listener will execute once the specified event is triggered
txtFirstName.addEventListener('keyup', (event) => {
    // The "innerHTML" property sets or returns the HTML content 
    spanFullName.innerHTML = txtFirstName.value;
});

txtFirstName.addEventListener('keyup', (event) => {
    // The 'event.target' contains the element where the event happened
    console.log(event.target);

    // The 'event.target.value' gets the value of the input element
    console.log(event.target.value);
});